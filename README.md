# Case Study Innoscripta - Anjar Daniswara

### 1. Gitlab Settings
- [01 Download Project and Push to Gitlab](https://gitlab.com/anjardanis/steps-technical-test-innoscripta-anjar/-/tree/master/gitlab-settings/01%20Download%20Project%20and%20Push%20to%20Gitlab)
- [02 Invite Person in Gitlab Project](https://gitlab.com/anjardanis/steps-technical-test-innoscripta-anjar/-/tree/master/gitlab-settings/02%20Invite%20Person%20in%20Gitlab%20Project)
- [03 Create Dockerfile and enable CI/CD Gitlab](https://gitlab.com/anjardanis/steps-technical-test-innoscripta-anjar/-/tree/master/gitlab-settings/03%20Create%20Dockerfile%20and%20enable%20CICD)


### 2. Gitlab CI/CD and Deploy to EKS in ECR
- [04 Run Unit Test in Merge Request and Block if Failed](https://gitlab.com/anjardanis/steps-technical-test-innoscripta-anjar/-/tree/master/gitlab-cicd/04%20Run%20Unit%20Test%20in%20Merge%20Request%20and%20Block%20if%20Failed)
- [05 Create Gitlab CI/CD that deploy in EKS with ECR](https://gitlab.com/anjardanis/steps-technical-test-innoscripta-anjar/-/tree/master/gitlab-cicd/05%20Create%20Gitlab%20CICD%20that%20deploy%20in%20EKS%20with%20ECR)


![Innoscripta-Technical-Test-Diagram](https://user-images.githubusercontent.com/73266070/189140902-c70a0580-252a-4d3d-851b-4ff92e06924a.jpeg)

#### Explanation
1. Start from Developer create Merge Request to Primary Branch (master)
2. Gitlab CI will trigger when Merge Request happen for Unit Test
3. Unit Test Pass can Merge commit if not Developer should fix the issue
4. if Merge Request approve and commit push to master branch Gitlab CI will build docker
5. after build docker will push to ECR
6. and the docker images will deploy to EKS AWS and EC2