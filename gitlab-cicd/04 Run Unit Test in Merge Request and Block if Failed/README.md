# Run sample tests on CI stage when MR is created. Make sure that failed test won’t allow MR to complete

* #### Modify `gitlab-ci.yml` 
```
default:
  image: php:7.4-fpm

before_script:
  - echo "Preparation Install Package"
  - export PROJECT_DIR=$(echo ${PWD})
  - echo $PROJECT_DIR
  - apt-get update
  - apt-get install -y --no-install-recommends zip unzip gnupg2 apt-utils git curl libzip-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev
  - curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

stages:
  - unit_test

unit_test:
  stage: unit_test
  script: 
    - echo "Build ....."
    - composer update
    - mv .env.example .env
    - echo "run unit test ...."
    - vendor/bin/phpunit
  only:
    - merge_requests
```

* #### Setting Merge Request in Gitlab Repository, Settings > Merge Requests
![01](assets/01.png)

* #### Checkmarks Pipelines must succeed
![03](assets/03.png)

* #### Create MR Dummy and the Result is
![04](assets/04.png)