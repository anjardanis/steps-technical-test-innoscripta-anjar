# GitLab CI/CD will deploy this repository to AWS EC2 using EKS and ECR when there is a merge to master branch 

* #### Create ECR and EKS by Terraform in AWS 
![05](assets/05.png)
![06](assets/06.png)
![07](assets/07.png)


* #### Add Node Group
![10](assets/10.png)
![09](assets/09.png)

* #### Add Variable AWS_ACCESS_KEY,AWS_SECRET_KEY_ACCESS, AWS_DEFAULT_REGION to repository
![08](assets/08.png)

* #### modify `gitlab-ci.yml`
```
stages:
  - unit_test
  - docker_build
  - deploy_services
  - deploy_app

unit_test:
  image: php:7.4-fpm
  stage: unit_test
  before_script:
    - echo "Preparation Install Package"
    - export PROJECT_DIR=$(echo ${PWD})
    - echo $PROJECT_DIR
    - apt-get update
    - apt-get install -y --no-install-recommends zip unzip gnupg2 apt-utils git curl libzip-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev
    - curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
  script: 
    - echo "Build ....."
    - composer update
    - mv .env.example .env
    - echo "run unit test ...."
    - vendor/bin/phpunit
  only:
    - merge_requests

docker_build:
  image: docker:stable
  stage: docker_build
  services:
    - docker:dind
  before_script:
    - export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    - export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
    - REPOSITORY_URI=119150223045.dkr.ecr.ap-southeast-1.amazonaws.com/anjar-ecr-innoscripta
    - echo "install awscli"
    - apk add --no-cache python3 py3-pip
    - pip3 install --no-cache-dir awscli
    - mv .env.example .env
  script:
    - aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 119150223045.dkr.ecr.ap-southeast-1.amazonaws.com
    - docker build -t $REPOSITORY_URI:${CI_COMMIT_SHORT_SHA} .
    - docker tag $REPOSITORY_URI:${CI_COMMIT_SHORT_SHA} $REPOSITORY_URI:latest
    - echo push latest Docker images to ECR...
    - docker push $REPOSITORY_URI:${CI_COMMIT_SHORT_SHA}
    - docker push $REPOSITORY_URI:latest
    - echo "DOCKER_AUTH_CONFIG={\"auths\":{\"119150223045.dkr.ecr.ap-southeast-1.amazonaws.com\":{\"auth\":\"`echo \"AWS:$(aws ecr get-login-password)\" | base64 | tr -d '\n'`\"}}}" >> ${CI_PROJECT_DIR}/auth.env
  only:
    refs:
      - master
  artifacts:
    reports:
      dotenv: auth.env


deploy_services:
  image: matshareyourscript/aws-helm-kubectl
  stage: deploy_services
  before_script:
    - export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    - export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
  script:
    - cd .kubernetes
    - kubectl apply -f services.yaml
  rules:
    - changes:
      - .kubernetes/services.yaml


deploy_app:
  image: matshareyourscript/aws-helm-kubectl
  stage: deploy_app
  before_script:
    - export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    - export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
  script:
    - cd .kubernetes
    - aws eks update-kubeconfig --name anjar-eks-innoscripta
    - sed -i 's/<VERSION>/'${CI_COMMIT_SHORT_SHA}'/g' deployment.yaml
    - kubectl apply -f deployment.yaml

```

* #### Result:
![11](assets/11.png)
![12](assets/12.png)
![13](assets/13.png)