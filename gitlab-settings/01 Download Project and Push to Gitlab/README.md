# Download this project from Github: https://github.com/SchuBu/new-laravelproject

* #### Clone Project into your local by command by command `git clone https://github.com/SchuBu/new-laravel-project.git`
![01](assets/01.png)
![02](assets/02.png)


# Upload it to your personal GitLab account and invite febrianto@innoscripta.com as a developer to your GitLab repository

* #### Create New Repo in Gitlab
![03](assets/03.png)


* #### Change remote-url to gitlab and push to gitlab
```
git remote set-url origin git@gitlab.com:anjardanis/anjar-innoscripta-laravel.git
git add .
git commit -am "first initial commit"
git push origin master
```
![04](assets/04.png)
![05](assets/05.png)




